%
% Author        : Yannis Beaux (Kranck)
% Date          : 15/11/2018
% Description   : Exercice 2 pour le Devoir 1 de MOIA (un element sur deux d'un tableau)
%

% Deux fonctions simple permettant de savoir si un element est pair ou impair
is_pair(X):-0 is X mod 2.
is_not_pair(X):-not(is_pair(X)).

% Retourne tous les éléments dont le rang est impair d'une liste donnée
% On prende l'élément au rang 1, 3 , 5 , 7 , 9 etc...
% On aurrait pu utiliser nth0 au lieu de nth1 et prendre tous les elements pairs : 
% 0 , 2 , 4 , 6 etc..
get_one_on_two_elements([X | Y] , R):-
        % On recupere l'element a l'index Z dans le tableau, qu'on stocke dans R
        nth1(Z , [X | Y] , R)
        % On test si l'index Z est pair
    ,   is_not_pair(Z).

% Extrait un element sur deux d'un tableau donné
extract([X | Y] , L):-
        % On fait un findall sur la liste
        % Z contiendra la solution de get_one_on_two_elements et sera ajouté a la liste L 
        % Exemple : extract([k,a,y,ak], L) => L = [k,y,k]
        findall(Z , get_one_on_two_elements([X | Y] , Z) , L).


% ---- TESTS ----
:-begin_tests(extract).
    % Tests de Julien
    test('extract([1,2,3],[1,3]).'):- extract([1,2,3],[1,3]).
    test('extract([1],[1]).'):- extract([1],[1]).
    test('extract([a,b,c,d],[a,b]).',fail):- extract([a,b,c,d],[a,b]).
:-end_tests(extract).