%
% Author        : Yannis Beaux (Kranck)
% Date          : 08/11/2018
% Description   : Exercice 1 pour le Devoir 1 de MOIA (palindrome)
%

% Crée un palindrome de la liste passé en argument 1
% Exemple : creerPalindrome([k,a,y],P). => P = [k, a, y, a, k]
creerPalindrome([X | Y] , Palindrome):-
        % On récupére la liste (sauf le dernier élément) renversée
        % Cette liste sera simplement ajoutée a la fin de la liste de base pour créer le palindrome
        all_but_not_last_reversed([X | Y] , Revertedstart)
        % On ajoute la liste inversée au bout de la liste originale
    ,   append([X | Y] , Revertedstart , Palindrome).

% Permet de renvoyer la seconde partie de la liste (donc toute la liste sauf le premier element)
get_end_of_table([X | Y] , Y).

% Retourne toute la liste sauf le dernier élément, la liste renvoyée est inversée
all_but_not_last_reversed([X | Y] , All):-
        % On inverse la liste
        reverse([X | Y] , Reverted)
        % On recupere la fin de la table 
    ,   get_end_of_table(Reverted , All).