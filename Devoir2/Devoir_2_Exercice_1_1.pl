%
% Author        : Yannis Beaux (Kranck)
% Date          : 06/12/2018
% Description   : Exercice 1 , question 1 , pour le devoir 2 de MOIA (bungalows)
%

% TODO: Comment
elementMembre([],_).
elementMembre([X | Y] , L2):-
        select(X , L2 , L)
    ,   elementMembre(Y , L).


% TODO: Comment
solution(S):-
    % 1.
        S = [bung(alsace , H1 , N1 , F1) , bung(bourgogne , H2 , N2 , F2) , bung(centre , H3 , N3 , F3)
            ,bung(limousin , H4 , N4 , F4) , bung(picardie , H5 , N5 , F5)]
    % 2,3,4
    ,   elementMembre([H1 , H2 , H3 , H4 , H5] , [9.5 , 11 , 13 , 15 , 16.5])
    ,   elementMembre([N1 , N2 , N3 , N4 , N5] , [9 , 12 , 17 , 20 , 24])
    ,   elementMembre([F1 , F2 , F3 , F4 , F5] , [2 , 3 , 4 , 5 , 6])
    % 5. Les Bourguignons sont arriv´es deux heures apr`es la famille compos´ee de 4 membres.
    ,   member(bung(bourgogne , HeureArriveeBourgogne , _ , _) , S)
    ,   member(bung(_ , HeureArriveFamille4 , _ , 4) , S)
    ,   HeureArriveeBourgogne is HeureArriveFamille4 + 2
    % 6. Les Bourguignons sont arriv´es avant les Picards.
    ,   member(bung(bourgogne , HeureArriveeBourgogne2 , _ , _) , S)
    ,   member(bung(picardie , HeureArriveePicardie , _ , _) , S)
    ,   HeureArriveeBourgogne2 < HeureArriveePicardie
    % 7. Les Picards sont log´es au num´ero 9.
    ,   N5 = 9
    % 8. Les Picards ne sont pas trois.
    ,   F5 \= 3
    % 9. Deux personnes occupent le bungalow num´ero 12 et elles ne sont pas Alsaciennes ni
    % Bourguignones.
    ,   member(bung(RegionBungalow12,_,12,2),S)
    ,   RegionBungalow12 \= alsace
    ,   RegionBungalow12 \= bourgogne
    % 10. La famille compos´ee de 5 membres est log´ee au num´ero 24. Elle est arriv´ee juste avant
    % les Alsaciens.
    ,   member(bung(_ , HeureArriveFamille5 , 24 , 5) , S)
    ,   member(bung(alsace , HeureArriveeAlsace , _ , _) , S)
    ,   HeureArriveFamille5 < HeureArriveeAlsace
    % 11. Les Alsaciens ne sont pas quatre.
    ,   F1 \= 4
    % 12. Les Limousins occupent le bungalow num´ero 17 et
    ,   N4 = 17
    % 13. Les Limousins sont arriv´es `a 11h.
    ,   H4 = 11.